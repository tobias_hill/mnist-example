# mnist-example

Testing the classical dataset MNIST dataset on my [neural network implementation](https://bitbucket.org/tobias_hill/neuralnet/src/master/).

## Authors

* **Tobias Hill** - [Machine Learning blog](https://ml.tobiashill.se)

 